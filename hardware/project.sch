EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ftdi
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FT232RL U1
U 1 1 55642D22
P 4900 3600
F 0 "U1" H 4250 4500 50  0000 L CNN
F 1 "FT232RL" H 5300 4500 50  0000 L CNN
F 2 "Housings_SSOP:SSOP-28_5.3x10.2mm_Pitch0.65mm" H 4900 3500 60  0001 C CNN
F 3 "" H 4900 3500 60  0000 C CNN
	1    4900 3600
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X05 P1
U 1 1 55642EFA
P 2300 3200
F 0 "P1" H 2300 3500 50  0000 C CNN
F 1 "USB" V 2400 3200 50  0000 C CNN
F 2 "local:USBPCB" H 2300 3200 60  0001 C CNN
F 3 "" H 2300 3200 60  0000 C CNN
	1    2300 3200
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 5564322F
P 3700 2000
F 0 "P2" H 3700 2200 50  0000 C CNN
F 1 "VSEL" V 3800 2000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 3700 2000 60  0001 C CNN
F 3 "" H 3700 2000 60  0000 C CNN
	1    3700 2000
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR01
U 1 1 55643361
P 2700 2900
F 0 "#PWR01" H 2700 2750 60  0001 C CNN
F 1 "+5V" H 2700 3040 60  0000 C CNN
F 2 "" H 2700 2900 60  0000 C CNN
F 3 "" H 2700 2900 60  0000 C CNN
	1    2700 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5564337B
P 2600 3500
F 0 "#PWR02" H 2600 3250 60  0001 C CNN
F 1 "GND" H 2600 3350 60  0000 C CNN
F 2 "" H 2600 3500 60  0000 C CNN
F 3 "" H 2600 3500 60  0000 C CNN
	1    2600 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3100 2600 3100
Wire Wire Line
	2600 3000 2600 3500
Wire Wire Line
	2600 3000 2500 3000
Connection ~ 2600 3100
Wire Wire Line
	2500 3400 2700 3400
Wire Wire Line
	2700 3400 2700 2900
Wire Wire Line
	2500 3200 4100 3200
Wire Wire Line
	2500 3300 4100 3300
Wire Wire Line
	4000 2900 4100 2900
Wire Wire Line
	4000 2100 4000 2900
Wire Wire Line
	4000 2100 3900 2100
Wire Wire Line
	4800 2000 4800 2600
Wire Wire Line
	3900 2000 6400 2000
Wire Wire Line
	3900 1900 4000 1900
Wire Wire Line
	4000 1900 4000 1800
$Comp
L +5V #PWR03
U 1 1 55643B25
P 4000 1800
F 0 "#PWR03" H 4000 1650 60  0001 C CNN
F 1 "+5V" H 4000 1940 60  0000 C CNN
F 2 "" H 4000 1800 60  0000 C CNN
F 3 "" H 4000 1800 60  0000 C CNN
	1    4000 1800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 55643D54
P 5000 2500
F 0 "#PWR04" H 5000 2350 60  0001 C CNN
F 1 "+5V" H 5000 2640 60  0000 C CNN
F 2 "" H 5000 2500 60  0000 C CNN
F 3 "" H 5000 2500 60  0000 C CNN
	1    5000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2500 5000 2600
Wire Wire Line
	5100 4700 5100 4600
Wire Wire Line
	4000 4700 5100 4700
Wire Wire Line
	5000 4700 5000 4600
Wire Wire Line
	4900 4700 4900 4600
Connection ~ 5000 4700
Wire Wire Line
	4700 4600 4700 4800
Connection ~ 4900 4700
Wire Wire Line
	4000 4700 4000 4300
Wire Wire Line
	4000 4300 4100 4300
Connection ~ 4700 4700
$Comp
L GND #PWR05
U 1 1 55643E4F
P 4700 4800
F 0 "#PWR05" H 4700 4550 60  0001 C CNN
F 1 "GND" H 4700 4650 60  0000 C CNN
F 2 "" H 4700 4800 60  0000 C CNN
F 3 "" H 4700 4800 60  0000 C CNN
	1    4700 4800
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 55643F51
P 3100 4000
F 0 "C1" H 3150 4100 50  0000 L CNN
F 1 "100n" H 3150 3900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3138 3850 30  0001 C CNN
F 3 "" H 3100 4000 60  0000 C CNN
	1    3100 4000
	1    0    0    -1  
$EndComp
$Comp
L CP2 C2
U 1 1 55643F74
P 3500 4000
F 0 "C2" H 3550 4100 50  0000 L CNN
F 1 "4u7" H 3550 3900 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_Reflow" H 3538 3850 30  0001 C CNN
F 3 "" H 3500 4000 60  0000 C CNN
	1    3500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3800 3100 3700
Wire Wire Line
	3100 3700 3500 3700
Wire Wire Line
	3500 3700 3500 3800
Wire Wire Line
	3100 4200 3100 4300
Wire Wire Line
	3100 4300 3500 4300
Wire Wire Line
	3500 4300 3500 4200
Wire Wire Line
	3300 4300 3300 4400
Connection ~ 3300 4300
Wire Wire Line
	3300 3700 3300 3600
Connection ~ 3300 3700
$Comp
L +5V #PWR06
U 1 1 5564419D
P 3300 3600
F 0 "#PWR06" H 3300 3450 60  0001 C CNN
F 1 "+5V" H 3300 3740 60  0000 C CNN
F 2 "" H 3300 3600 60  0000 C CNN
F 3 "" H 3300 3600 60  0000 C CNN
	1    3300 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 556441B1
P 3300 4400
F 0 "#PWR07" H 3300 4150 60  0001 C CNN
F 1 "GND" H 3300 4250 60  0000 C CNN
F 2 "" H 3300 4400 60  0000 C CNN
F 3 "" H 3300 4400 60  0000 C CNN
	1    3300 4400
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 55644AC3
P 3600 2500
F 0 "C3" H 3650 2600 50  0000 L CNN
F 1 "100n" H 3650 2400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 3638 2350 30  0001 C CNN
F 3 "" H 3600 2500 60  0000 C CNN
	1    3600 2500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 55644B78
P 3600 2800
F 0 "#PWR08" H 3600 2550 60  0001 C CNN
F 1 "GND" H 3600 2650 60  0000 C CNN
F 2 "" H 3600 2800 60  0000 C CNN
F 3 "" H 3600 2800 60  0000 C CNN
	1    3600 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2200 3600 2200
Wire Wire Line
	3600 2200 3600 2300
Connection ~ 4000 2200
Wire Wire Line
	3600 2700 3600 2800
Wire Wire Line
	5700 4200 6400 4200
Text GLabel 4800 2000 1    60   Input ~ 0
VCCIO
$Comp
L R R1
U 1 1 556452CA
P 6400 2350
F 0 "R1" V 6480 2350 50  0000 C CNN
F 1 "10K" V 6407 2351 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6330 2350 30  0001 C CNN
F 3 "" H 6400 2350 30  0000 C CNN
	1    6400 2350
	-1   0    0    1   
$EndComp
Connection ~ 4800 2000
$Comp
L C C4
U 1 1 556454FC
P 6000 2300
F 0 "C4" H 6050 2400 50  0000 L CNN
F 1 "100n" H 6050 2200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6038 2150 30  0001 C CNN
F 3 "" H 6000 2300 60  0000 C CNN
	1    6000 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2000 6000 2100
Wire Wire Line
	6000 2500 6000 2600
$Comp
L GND #PWR09
U 1 1 556455EA
P 6000 2600
F 0 "#PWR09" H 6000 2350 60  0001 C CNN
F 1 "GND" H 6000 2450 60  0000 C CNN
F 2 "" H 6000 2600 60  0000 C CNN
F 3 "" H 6000 2600 60  0000 C CNN
	1    6000 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3900 6600 3900
Wire Wire Line
	5700 4000 7000 4000
$Comp
L LED D1
U 1 1 55645B95
P 6600 4600
F 0 "D1" H 6600 4700 50  0000 C CNN
F 1 "TX" H 6600 4500 50  0000 C CNN
F 2 "LEDs:LED-0603" H 6600 4600 60  0001 C CNN
F 3 "" H 6600 4600 60  0000 C CNN
	1    6600 4600
	0    -1   -1   0   
$EndComp
$Comp
L LED D2
U 1 1 55645E25
P 7000 4600
F 0 "D2" H 7000 4700 50  0000 C CNN
F 1 "RX" H 7000 4500 50  0000 C CNN
F 2 "LEDs:LED-0603" H 7000 4600 60  0001 C CNN
F 3 "" H 7000 4600 60  0000 C CNN
	1    7000 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6600 3900 6600 4400
Wire Wire Line
	7000 4000 7000 4400
Wire Wire Line
	6600 4800 6600 4900
Wire Wire Line
	7000 4800 7000 4900
$Comp
L R R2
U 1 1 55645FC8
P 6600 5150
F 0 "R2" V 6680 5150 50  0000 C CNN
F 1 "270" V 6607 5151 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6530 5150 30  0001 C CNN
F 3 "" H 6600 5150 30  0000 C CNN
	1    6600 5150
	-1   0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 556460CD
P 7000 5150
F 0 "R3" V 7080 5150 50  0000 C CNN
F 1 "270" V 7007 5151 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6930 5150 30  0001 C CNN
F 3 "" H 7000 5150 30  0000 C CNN
	1    7000 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6600 5400 6600 5500
Wire Wire Line
	6600 5500 7000 5500
Wire Wire Line
	7000 5500 7000 5400
Wire Wire Line
	6800 5500 6800 5600
Connection ~ 6800 5500
$Comp
L +5V #PWR010
U 1 1 556462A3
P 6800 5600
F 0 "#PWR010" H 6800 5450 60  0001 C CNN
F 1 "+5V" H 6800 5740 60  0000 C CNN
F 2 "" H 6800 5600 60  0000 C CNN
F 3 "" H 6800 5600 60  0000 C CNN
	1    6800 5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	6400 2000 6400 2100
Connection ~ 6000 2000
Wire Wire Line
	6400 4200 6400 2600
Wire Wire Line
	5700 4300 5800 4300
$Comp
L CONN_01X08 P3
U 1 1 55647992
P 7700 3250
F 0 "P3" H 7700 3700 50  0000 C CNN
F 1 "UART" V 7800 3250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x08" H 7700 3250 60  0001 C CNN
F 3 "" H 7700 3250 60  0000 C CNN
	1    7700 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2900 5800 2900
Wire Wire Line
	5700 3000 5800 3000
Wire Wire Line
	5700 3100 5800 3100
Wire Wire Line
	5700 3200 5800 3200
Wire Wire Line
	5700 3300 5800 3300
Wire Wire Line
	5700 3400 5800 3400
Wire Wire Line
	5700 3500 5800 3500
Wire Wire Line
	5700 3600 5800 3600
Wire Wire Line
	7400 2900 7500 2900
Wire Wire Line
	7500 3000 7400 3000
Wire Wire Line
	7400 3100 7500 3100
Wire Wire Line
	7400 3200 7500 3200
Wire Wire Line
	7400 3300 7500 3300
Wire Wire Line
	7400 3400 7500 3400
Wire Wire Line
	7400 3500 7500 3500
Wire Wire Line
	7400 3600 7500 3600
Text Label 5800 2900 0    60   ~ 0
TXD
Text Label 5800 3000 0    60   ~ 0
RXD
Text Label 5800 3100 0    60   ~ 0
RTS
Text Label 5800 3200 0    60   ~ 0
CTS
Text Label 5800 3300 0    60   ~ 0
DTR
Text Label 5800 3400 0    60   ~ 0
DCR
Text Label 5800 3500 0    60   ~ 0
DCD
Text Label 5800 3600 0    60   ~ 0
RI
Text Label 7400 2900 2    60   ~ 0
TXD
Text Label 7400 3000 2    60   ~ 0
DTR
Text Label 7400 3100 2    60   ~ 0
RTS
Text Label 7400 3200 2    60   ~ 0
RXD
Text Label 7400 3300 2    60   ~ 0
RI
Text Label 7400 3400 2    60   ~ 0
DCR
Text Label 7400 3500 2    60   ~ 0
DCD
Text Label 7400 3600 2    60   ~ 0
CTS
Wire Wire Line
	5700 4100 5800 4100
Text Label 5800 4100 0    60   ~ 0
CBUS2
Text Label 5800 3900 0    60   ~ 0
CBUS0
Text Label 5800 4000 0    60   ~ 0
CBUS1
Text Label 5800 4200 0    60   ~ 0
CBUS3
Text Label 5800 4300 0    60   ~ 0
CBUS4
$Comp
L CONN_01X05 P4
U 1 1 55649DDA
P 7700 4100
F 0 "P4" H 7700 4400 50  0000 C CNN
F 1 "CBUS" V 7800 4100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x05" H 7700 4100 60  0001 C CNN
F 3 "" H 7700 4100 60  0000 C CNN
	1    7700 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4300 7400 4300
Wire Wire Line
	7500 4200 7400 4200
Wire Wire Line
	7400 4100 7500 4100
Wire Wire Line
	7400 4000 7500 4000
Wire Wire Line
	7400 3900 7500 3900
Text Label 7400 4200 2    60   ~ 0
CBUS0
Text Label 7400 4300 2    60   ~ 0
CBUS1
Text Label 7400 4000 2    60   ~ 0
CBUS2
Text Label 7400 4100 2    60   ~ 0
CBUS3
Text Label 7400 3900 2    60   ~ 0
CBUS4
$Comp
L CONN_01X02 P5
U 1 1 5564E9A7
P 8700 3150
F 0 "P5" H 8700 3300 50  0000 C CNN
F 1 "5V" V 8800 3150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02" H 8700 3150 60  0001 C CNN
F 3 "" H 8700 3150 60  0000 C CNN
	1    8700 3150
	1    0    0    1   
$EndComp
$Comp
L CONN_01X02 P6
U 1 1 5564EA96
P 8700 4050
F 0 "P6" H 8700 4200 50  0000 C CNN
F 1 "3V3" V 8800 4050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x02" H 8700 4050 60  0001 C CNN
F 3 "" H 8700 4050 60  0000 C CNN
	1    8700 4050
	1    0    0    1   
$EndComp
Wire Wire Line
	8500 3100 8400 3100
Wire Wire Line
	8400 3100 8400 3000
Wire Wire Line
	8500 3200 8400 3200
Wire Wire Line
	8400 3200 8400 3300
$Comp
L GND #PWR011
U 1 1 5564FDDD
P 8400 3300
F 0 "#PWR011" H 8400 3050 60  0001 C CNN
F 1 "GND" H 8400 3150 60  0000 C CNN
F 2 "" H 8400 3300 60  0000 C CNN
F 3 "" H 8400 3300 60  0000 C CNN
	1    8400 3300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR012
U 1 1 5564FF7F
P 8400 3000
F 0 "#PWR012" H 8400 2850 60  0001 C CNN
F 1 "+5V" H 8400 3140 60  0000 C CNN
F 2 "" H 8400 3000 60  0000 C CNN
F 3 "" H 8400 3000 60  0000 C CNN
	1    8400 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 4000 8400 4000
Wire Wire Line
	8400 4000 8400 3900
Wire Wire Line
	8500 4100 8400 4100
Wire Wire Line
	8400 4100 8400 4200
$Comp
L GND #PWR013
U 1 1 556508BB
P 8400 4200
F 0 "#PWR013" H 8400 3950 60  0001 C CNN
F 1 "GND" H 8400 4050 60  0000 C CNN
F 2 "" H 8400 4200 60  0000 C CNN
F 3 "" H 8400 4200 60  0000 C CNN
	1    8400 4200
	1    0    0    -1  
$EndComp
Text GLabel 4000 2200 2    60   Input ~ 0
3V3REF
$Comp
L LD1117S33TR U2
U 1 1 556525E2
P 8900 4950
F 0 "U2" H 8900 5200 40  0000 C CNN
F 1 "LD1117S33TR" H 8900 5150 40  0000 C CNN
F 2 "SMD_Packages:SOT-223" H 8900 5050 40  0001 C CNN
F 3 "" H 8900 4950 60  0000 C CNN
	1    8900 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 55652854
P 8900 5300
F 0 "#PWR014" H 8900 5050 60  0001 C CNN
F 1 "GND" H 8900 5150 60  0000 C CNN
F 2 "" H 8900 5300 60  0000 C CNN
F 3 "" H 8900 5300 60  0000 C CNN
	1    8900 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5300 8900 5200
Wire Wire Line
	8500 4900 8400 4900
Wire Wire Line
	8400 4900 8400 4800
$Comp
L +5V #PWR015
U 1 1 55652ADD
P 8400 4800
F 0 "#PWR015" H 8400 4650 60  0001 C CNN
F 1 "+5V" H 8400 4940 60  0000 C CNN
F 2 "" H 8400 4800 60  0000 C CNN
F 3 "" H 8400 4800 60  0000 C CNN
	1    8400 4800
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR016
U 1 1 55652B49
P 9400 4800
F 0 "#PWR016" H 9400 4650 60  0001 C CNN
F 1 "+3V3" H 9400 4940 60  0000 C CNN
F 2 "" H 9400 4800 60  0000 C CNN
F 3 "" H 9400 4800 60  0000 C CNN
	1    9400 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 4900 9400 4900
Wire Wire Line
	9400 4900 9400 4800
$Comp
L +3V3 #PWR017
U 1 1 55652C41
P 8400 3900
F 0 "#PWR017" H 8400 3750 60  0001 C CNN
F 1 "+3V3" H 8400 4040 60  0000 C CNN
F 2 "" H 8400 3900 60  0000 C CNN
F 3 "" H 8400 3900 60  0000 C CNN
	1    8400 3900
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 55654167
P 9800 5100
F 0 "C5" H 9850 5200 50  0000 L CNN
F 1 "100n" H 9850 5000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 9838 4950 30  0001 C CNN
F 3 "" H 9800 5100 60  0000 C CNN
	1    9800 5100
	1    0    0    -1  
$EndComp
$Comp
L CP2 C6
U 1 1 5565416D
P 10200 5100
F 0 "C6" H 10250 5200 50  0000 L CNN
F 1 "4u7" H 10250 5000 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_Reflow" H 10238 4950 30  0001 C CNN
F 3 "" H 10200 5100 60  0000 C CNN
	1    10200 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 4900 9800 4800
Wire Wire Line
	9800 4800 10200 4800
Wire Wire Line
	10200 4800 10200 4900
Wire Wire Line
	9800 5300 9800 5400
Wire Wire Line
	9800 5400 10200 5400
Wire Wire Line
	10200 5400 10200 5300
Wire Wire Line
	10000 5400 10000 5500
Connection ~ 10000 5400
Wire Wire Line
	10000 4800 10000 4700
Connection ~ 10000 4800
$Comp
L GND #PWR018
U 1 1 55654183
P 10000 5500
F 0 "#PWR018" H 10000 5250 60  0001 C CNN
F 1 "GND" H 10000 5350 60  0000 C CNN
F 2 "" H 10000 5500 60  0000 C CNN
F 3 "" H 10000 5500 60  0000 C CNN
	1    10000 5500
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR019
U 1 1 55654208
P 10000 4700
F 0 "#PWR019" H 10000 4550 60  0001 C CNN
F 1 "+3V3" H 10000 4840 60  0000 C CNN
F 2 "" H 10000 4700 60  0000 C CNN
F 3 "" H 10000 4700 60  0000 C CNN
	1    10000 4700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
